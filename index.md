# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## pour supprimer des feuilles Excel et créer une feuille en Excel

Appuyer sur la feuille que nous souhaitons supprimer, clique-droit avec la souris (un dialogue s'ouvre en nous prévenant des conséquences de cet acte puis appuyer sur ok pour valider la suppression de la feuille).

En excel, il existe juste à côté du menu des feuilles et il faut appuyer sur + pour ajouter une feuille.

## pour ajouter des données dans un tableur excel

Il faut simplement ajouter une donnée par exemple le mois de janvier et en prenant la colonne A qui nous sert d'exemple et où se trouve la donnée du mois de Janvier, nous prenons la colonne A1 et en bas à droite, nous devons cliquer dessus, puis une croix directionnelle se déclenche.

Nous la prenons puis nous la glissons jusqu'à la colonne A12 et cela nous permet d'avoir les douze mois de l'année sans avoir rentrer des données à la main.

Pour supprimer plusieurs onglets:

Il faut prendre les colonnes à supprimer en maintenant la touche Ctrl enfoncée tout en prenant les lignes à supprimer , clique-droit et appuyer sur supprimer qui ouvre le dialogue lignes et les lignes sélectionnées ont être supprimées.

## pour supprimer des colonnes:

même principe que pour les lignes: on sélectionne les colonnes demandées par l'exercice puis dans le dialogue supprimer, nous appuyons sur colonnes à la place de lignes.

Les colonnes sélectionnées ont été supprimées conformément à l'exercice demandé.

## prendre plusieurs colonnes en même temps et les coloriser:

Appuyer sur le bouton ctrl enfoncé puis choisir les colonnes conformément à l'exercice puis clic droit et choisir couleur de remplissage. Les colonnes choisies seront de la couleur choisies.

## même principe que précédemment pour d'autres colonnes:

Prendre les colonnes que nous souhaitant coloriser tout en maintenant le bouton Ctrl puis faire clic droit, choisir la colonne que l'on veut coloriser de son choix tout en prenant la couleur de son choix avec l'onglet couleur de remplissage.
